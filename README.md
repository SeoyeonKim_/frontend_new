# Dockerized-NginX-NextJS

Make your NextJS app in /nextjs and run in real server or aws with nginx without terrible configuration.

## Docker

Start

```sh
docker-compose up -d && docker-compose logs -f
or
npm run docker:up
```

Stop

```sh
docker-compose stop
or
npm run docker:stop
```



### Docker Compose

```yml
version: '3.7'
services:
  nextjs:
    # container_name: nextjs
    image: node:16.2.0-alpine3.13
    ports:
      - "3000"
    volumes:
      - ../nextjs:/app/nextjs
      - ./logs/nextjs/npm:/root/.npm/_logs
    working_dir: /app/nextjs
    command: >
      /bin/sh -c "npm install -f&&
        npm run build &&
        npm run start"
  nginx:
    # container_name: nginx
    image: nginx:1.15.8-alpine
    ports:
      - '80:80'
      - '443:443'
    volumes:
      - ./nginx:/etc/nginx/conf.d
      - ./logs/nginx:/var/log/nginx
    links:
      - nextjs
    restart: always
```